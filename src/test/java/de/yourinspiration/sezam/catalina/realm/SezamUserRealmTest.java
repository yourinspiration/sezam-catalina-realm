package de.yourinspiration.sezam.catalina.realm;

/*
 * #%L
 * sezam-catalina-realm
 * %%
 * Copyright (C) 2014 Yourinspiration
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.Principal;

import org.apache.catalina.realm.GenericPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.yourinspiration.sezam.java.sdk.Account;
import de.yourinspiration.sezam.java.sdk.Service;

/**
 * Test case for {@link SezamUserRealm}.
 * 
 * @author Marcel Härle
 * 
 */
public class SezamUserRealmTest {

    private SezamUserRealm sezamUserRealm;
    @Mock
    private Service service;

    private Account account;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        sezamUserRealm = new SezamUserRealm();
        sezamUserRealm.setService(service);

        account = new Account();
        account.setUsername("max");
        account.setPassword("1234");
        account.setRoles("USER, ADMIN");
    }

    @Test
    public void testGetName() {
        String name = sezamUserRealm.getName();
        assertEquals(SezamUserRealm.REALM_NAME, name);
    }

    @Test
    public void testGetPassword() {
        Mockito.when(service.findByUsername(account.getUsername())).thenReturn(
                account);

        String password = sezamUserRealm.getPassword(account.getUsername());
        assertEquals(account.getPassword(), password);
    }

    @Test
    public void testGetPrincipal() {
        Mockito.when(service.findByUsername(account.getUsername())).thenReturn(
                account);

        Principal principal = sezamUserRealm
                .getPrincipal(account.getUsername());
        assertEquals(GenericPrincipal.class, principal.getClass());

        GenericPrincipal genericPrincipal = (GenericPrincipal) principal;

        assertEquals(account.getUsername(), genericPrincipal.getName());
        assertEquals(account.getPassword(), genericPrincipal.getPassword());
        assertTrue(genericPrincipal.getRoles().length == 2);
    }

}
