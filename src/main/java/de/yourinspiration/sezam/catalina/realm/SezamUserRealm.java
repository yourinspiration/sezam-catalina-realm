package de.yourinspiration.sezam.catalina.realm;

/*
 * #%L
 * sezam-catalina-realm
 * %%
 * Copyright (C) 2014 Yourinspiration
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.JDBCRealm;
import org.apache.catalina.realm.RealmBase;
import org.apache.catalina.realm.UserDatabaseRealm;

import de.yourinspiration.sezam.java.sdk.Account;
import de.yourinspiration.sezam.java.sdk.Service;
import de.yourinspiration.sezam.java.sdk.ServiceFactory;

/**
 * <p>
 * Implementation of the Catalina Security Realm. Can be used in the same manner
 * as the {@link JDBCRealm} or {@link UserDatabaseRealm} in a Catalina context.
 * </p>
 * <p>
 * Authenticates a user against the Sezam user management service.
 * </p>
 * <p>
 * Needs a <strong>sezam.properties</strong> file on the classpath.
 * </p>
 * 
 * @author Marcel Härle
 * 
 */
public class SezamUserRealm extends RealmBase {

    /**
     * The name of the realm for use in log messages.
     */
    public static final String REALM_NAME = "SezamUserRealm";

    private Service service;

    /**
     * Constructs a new object initialized with the default sezam user service.
     */
    public SezamUserRealm() {
        this.service = new ServiceFactory().getInstance();
    }

    @Override
    protected String getName() {
        return REALM_NAME;
    }

    @Override
    protected String getPassword(String username) {
        final Account account = service.findByUsername(username);
        if (account != null) {
            return account.getPassword();
        } else {
            return "";
        }
    }

    @Override
    protected Principal getPrincipal(String username) {
        final Account account = service.findByUsername(username);
        if (account != null) {
            List<String> roles = new ArrayList<>();
            String[] tokens = account.getRoles().split(",");
            for (String role : tokens) {
                roles.add(role);
            }
            // Use a GenericPrincipal in order to set the roles of the current
            // user.
            GenericPrincipal principal = new GenericPrincipal(
                    account.getUsername(), account.getPassword(), roles);
            return principal;
        } else {
            return null;
        }
    }

    /**
     * Get the current service.
     * 
     * @return the current service
     */
    public Service getService() {
        return service;
    }

    /**
     * Sets the service.
     * 
     * @param service
     *            the service to be set
     */
    public void setService(Service service) {
        this.service = service;
    }

}
